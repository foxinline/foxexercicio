package vendas.fox;
import java.util.*;
import java.math.*;
import java.io.*;
/**
 *
 * Candidato 1 - publicou projeto no github
 * 
 */
public class VendasFOX {
    public static void main(String[] args) throws IOException, ClassNotFoundException{
       VendasFOX self = new VendasFOX(); 
       File db = new File("vendas_db.bin"); //Arquivo onde serão salvos os dados
       ArrayList<vendedor> vendedores;
       boolean ok = db.createNewFile(); //Teste para ver se o arquivo existe
       if (ok){
           vendedores = new ArrayList<vendedor>(); //Caso o arquivo não existisse antes, inicializa-se uma nova lista
       } else {
           vendedores = self.load(db); //Se o arquivo já existe, ele deve conter dados que serão lidos na forma de uma lista
       }
       self.print("--------------------------------------------------"); //Separador
       self.print("\n1) Adicionar vendas.\n2) Exibir vendas.\n3) Exibir vendedores.\n4) Sair\n"); //Menu da aplicação
       int opcao = self.int_input("> "); //Lê a opção
       switch (opcao){ //Ações relacionadas a cada opção
           case 1:
               self.lerVendedores(vendedores, db);
               break;
           case 2:
               self.ShowVendas(vendedores);
               break;
           case 3:
               self.ShowVendedores(vendedores);
               break;
           default:
               return ; //Sai da aplicação
       }
       main(args); //Chamada para voltar ao inicio da aplicação (deverá se repetir até que o usuário selecione a opção SAIR no menu)
    }

    public void ShowVendedores(ArrayList<vendedor> vendedores){ //Funcao pra mostrar os vendedores cadastrados
        System.out.printf("%8s\t%8s\t%8s\t%8s\n\n", "Nome", "Nascimento", "CPF", "Cargo"); //Identificação formatada dos campos mostrados
        for (vendedor vd : vendedores)
            System.out.printf("%8s\t%8s\t%8s\t%8s\n", vd.getNome(), GetDateOnly(vd.getNascimento()), vd.getCpf(), vd.getCargo()); //Dados do vendedor
    }
    public void ShowVendas(ArrayList<vendedor> vendedores){ //Funcao pra mostrar as vendas
        System.out.printf("%8s\t%8s\t%8s\t%8s\t%8s\t%8s\n\n", "Venda (Descrição)","Data","Valor","Vendedor","Comissão (%)","Comissão (R$)"); //Identificação formatada dos campos mostrados
        for (vendedor vd : vendedores){
            for (venda vn : vd.getVendas()){
                System.out.printf("\t%8s\t%8s\t%8s\t%8s\t%8.2f\t%8.2f\n", vn.getDescricao(), GetDateOnly(vn.getDatavenda()), vn.getValor().toPlainString(), vd.getNome(), vn.getComissao(), vn.calculaComissao()); //Dados da venda
            }
        }
    }
    public String GetDateOnly(Date data){ //Retona a data formatada
        return data.getDay() + "/" + data.getMonth() + "/" + data.getYear();
    }
    public ArrayList<vendedor> load(File db) throws FileNotFoundException, IOException, ClassNotFoundException{ //Fucao para ler o arquivo com os dados
        try{   //Tratamento de erros
            FileInputStream fis = new FileInputStream(db);
            ObjectInputStream ois = new ObjectInputStream(fis);
            return (ArrayList<vendedor>) ois.readObject();
        } catch (Exception e){
            ArrayList<vendedor> nv = new ArrayList<vendedor>(); //Caso não seja possível ler o arquivo, a função retorna uma lista vazia
            return nv;
        }
    }
    public void save(ArrayList<vendedor> vendedores, File saveAs)throws FileNotFoundException, IOException{ //Funcao para salvar os dados sobre vendas e funcionarios
        FileOutputStream fos = new FileOutputStream(saveAs);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(vendedores);
    }
    public void lerVendedores(ArrayList<vendedor> vendedores, File db) throws FileNotFoundException, IOException{ //Função para ler os dados de vendedores (e suas vendas)
        String continuar;
        do {
            vendedor VD = lerVendedor();
            if (str_input("Adionar vendas deste funcionário? (S/N)").toUpperCase().compareTo("S") == 0){ //Verifica a necessidade de adicionar as vendas do funcionário
                lerVendas(VD); //Chamada para a função responsável por ler as informações sobre as vendas
            }
            vendedores.add(VD);
            continuar = str_input("Adicionar outro vendedor? (S/N)").toUpperCase();
        } while (continuar.compareTo("S") == 0); //Condição de saída do loop
        save(vendedores, db);
    }
    public void lerVendas(vendedor V){ //Adiciona as vendas do vendedor V
        String continuar;
        do {
            BigDecimal bd = new BigDecimal(double_input("Valor da venda: "));
            venda vn = new venda(bd, lerData("Data"), str_input("Descricao: "), double_input("Comissao (%): "));
            V.addVenda(vn);
            continuar = str_input("Deseja adicionar outra venda? (S/N) ").toUpperCase();
        } while (continuar.compareTo("S") == 0); //Verifica se há mais vendas a serem adicionadas
    }
    public vendedor lerVendedor(){ //Lê o dados do vendedor e os retorna
        vendedor vend = new vendedor(str_input("Nome do vendedor: "), lerData("Data de nascimento"), str_input("CPF: "), str_input("Cargo: "));
        return vend;
    }
    int atoi(String value){ //Função para converter uma string em sua representação inteira
        int v = 0, b=1;
        for (int i = value.length()-1; i>=0; i--){ //Pega cada caractere da String (de trás para frente)
            v += charToInt(value.charAt(i))*b; //Converte o caractere em inteiro e o multiplica pelo "peso" da casa decimal ocupada
            b*=10; //Aumenta o valor da casa decimal a esquerda
        }
        return v;
    }
    int charToInt(char ch){ //Converte um caractere para o número que este representa
        int num = ((int) ch) - 48; //Na tabela ascii, os numeros (0-9) começão na posição 48 (e vão até a 57)
        if (num >9) throw new NullPointerException(); //Só é realizada a conversão dos caracteres '0', ..., '9'
        return num;
    }
    public Date lerData(String prompt){ //Lê uma data (String) e a retorna como Date
        String dt[] = str_input(prompt + " (D/M/A): ").split("/"); //Divide a string lida para separá-la em separa em dia, mes e ano
        int d, m, a;
        d = atoi(dt[0]); //Converte para inteiro
        m = atoi(dt[1]); // --
        a = atoi(dt[2]); // --
        Date data = new Date(a, m, d); //Cria uma variavel Date com a data informada
        return data;
    }
    public int int_input(String prompt){ //Lê um inteiro do teclado
        print(prompt);
        Scanner s = new Scanner(System.in);
        return s.nextInt();
    }
    public String str_input(String prompt){ //Lê uma String do teclado
        print(prompt);
        Scanner s = new Scanner(System.in);
        return s.nextLine();
    }
    public double double_input(String prompt){ //Lê um valor double do teclado
        print(prompt);
        Scanner s = new Scanner(System.in);
        return s.nextDouble();
    }
    
    public void print(Object ... args){ //Escreve na saída padrão
        for (Object o : args)
            System.out.print(o);
    }
    
}
class venda implements Serializable{ //Classe venda deve implementar Serializable para que possa ser escrita em arquivo
    private BigDecimal valor;
    private Date datavenda;
    private String descricao;
    private double comissao;
    public venda(BigDecimal valor, Date datavenda, String descricao, double comissao){ //Construtor da classe
        this.valor = valor; //Inicializa as variáveis privadas (dados)
        this.datavenda = datavenda; 
        this.descricao = descricao;
        this.comissao = comissao;
    }

    public double getComissao() {
        return comissao;
    }
    public double calculaComissao(){
        return (this.comissao/100)*this.valor.doubleValue();
    }
    public Date getDatavenda() {
        return datavenda;
    }

    public String getDescricao() {
        return descricao;
    }

    public BigDecimal getValor() {
        return valor;
    }
}

class vendedor implements Serializable{ //Classe vendedor deve implementar Serializable para que possa ser escrita em arquivo
    private String nome, cargo, cpf;
    private Date nascimento;
    ArrayList <venda> Vendas;
    
    public Date getNascimento() {
        return nascimento;
    }
    
    public vendedor(String nome, Date nascimento, String cpf, String cargo){ //Construtor da classe
        this.nome = nome; //Inicializa as variáveis privadas (dados)
        this.nascimento = nascimento;
        this.cpf = cpf;
        this.cargo = cargo;
        this.Vendas = new ArrayList<venda>();
    }
   
    public ArrayList <venda> getVendas(){
        return this.Vendas;
    }

    public String getCargo() {
        return cargo;
    }

    public String getCpf() {
        return cpf;
    }

    public String getNome() {
        return nome;
    }
    public void addVenda(venda V){
        Vendas.add(V);
    }
}
