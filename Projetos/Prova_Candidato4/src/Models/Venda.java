/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author Candidato 4
 */
public class Venda {
    public int idVenda;
    public Double valor;
    // It should be Date, but now is String only to be fast at insert new records
    public String dataVenda;
    public String descricao;
    public Double comissao;
    public int idVendedor;
    // Constructor
    public Venda(int idVenda, Double valor, String dataVenda, String descricao, Double comissao, int idVendedor){
        this.idVenda = idVenda;
        this.valor = valor;
        this.dataVenda = dataVenda;
        this.descricao = descricao;
        this.comissao = comissao;
        this.idVendedor = idVendedor;
    }
}
