/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.util.Date;

/**
 *
 * @author Candidato 4
 */
public class Vendedor {
    public int idVendedor;
    public String nome;
    // It should be Date, but now is String only to be fast at insert new records
    public String dataNascimento;
    public String cpf;
    public String cargo;
    
    // Constructor
    public Vendedor(int idVendedor, String nome, String dataNascimento, String cpf, String cargo){
        this.idVendedor = idVendedor;
        this.nome = nome;
        this.dataNascimento = dataNascimento;
        this.cpf = cpf;
        this.cargo = cargo;
    }
}
