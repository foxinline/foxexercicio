/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Vendedor;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Candidato 4
 */
public class VendedorController {
    int idCount = 1;
    List<Vendedor> dados = new ArrayList<>();
    
    // Constructor
    public VendedorController(){
    }
    
    // Creates a new record
    public void inserirVendedor(String nome, String dataNascimento, String cpf, String cargo){
       Vendedor v = new Vendedor(idCount, nome, dataNascimento, cpf, cargo);
       idCount++;
       dados.add(v);
    }
    
    // List all records
    public List<Vendedor> listarVendedores(){
       System.out.println("\033[0;1midVendedor\tNome\tData de Nascimento\tCPF\tCargo\t");
       for(Vendedor v: dados){
           System.out.println(v.idVendedor + "\t" + v.nome + "\t" + "\t" + v.dataNascimento + "\t" + v.cpf + "\t" + v.cargo);
        }
       return dados;
    }
    
    // Reads a record
    public Vendedor procurarVendedor(int idVendedor){
       for(Vendedor v: dados){
           if(v.idVendedor == idVendedor){
               return v;
           }
        }
       return null;
    }
    
    // Update a existent record
    
    public void atualizarVendedor(int idVendedor, String nome, String dataNascimento, String cpf, String cargo){
       for(Vendedor v: dados){
           if(v.idVendedor == idVendedor){
               v.nome = nome;
               v.dataNascimento = dataNascimento;
               v.cpf = cpf;
               v.cargo = cargo;
           }
        }
    }
    
    // Deletes some Vendedor by id
    public int removerVendedor(int idVendedor){
       for(Vendedor v: dados){
           if(v.idVendedor == idVendedor){
               dados.remove(v);
               return 1;
           }
        }
       return 0;
    }
     // Verify if a passed Vendedor exists
    public boolean hasVendedor(int idVendedor){
       for(Vendedor v: dados){
           if(v.idVendedor == idVendedor){
               return true;
           }
        }
       return false;
    }
}
