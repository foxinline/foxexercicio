/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Venda;
import Models.Vendedor;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author foxinline
 */
public class VendaController {
    int idCount = 1;
    List<Venda> dados = new ArrayList<>();    
    VendedorController vendedorCtrl;
    // Constructor
    public VendaController(VendedorController vendedorCtrl) {
        this.vendedorCtrl = vendedorCtrl;
    }
    
    // Creates a new record
    public int inserirVenda(Double valor, String dataVenda, String descricao, Double comissao, int idVendedor){
       if (vendedorCtrl.hasVendedor(idVendedor)) {
           Venda v = new Venda(idCount++, valor, dataVenda, descricao, comissao, idVendedor);
           dados.add(v);
           return 1;
       } else {
           return 0;   
       }
    }
    
    // Reads a record
    public Venda procurarVenda(int idVenda){
       for(Venda v: dados){
           if(v.idVenda == idVenda){
               return v;
           }
        }
       return null;
    }
    
    // Update a existent record
    
    public void atualizarVenda(int idVenda, Double valor, String dataVenda, String descricao, Double comissao, int idVendedor){
       for(Venda v: dados){
           if(v.idVenda == idVenda){
               v.idVenda = idVenda;
               v.valor = valor;
               v.dataVenda = dataVenda;
               v.descricao = descricao;
               v.comissao = comissao;
               v.idVendedor = idVendedor;
           }
        }
    }
    
    // Deletes some Vendedor by id
    public int removerVenda(int idVenda){
       for(Venda v: dados){
           if(v.idVenda == idVenda){
               dados.remove(v);
               return 1;
           }
        }
       return 0;
    }
    
    // List Vendas by Vendedor
    public void listVendas(){
        System.out.println("\033[0;1mDescrição\tData\tValor\tVendedor\tComissão (%)\tComissão (R$)");
        for(Venda v: dados){
            Vendedor vendedor = vendedorCtrl.procurarVendedor(v.idVendedor);
            System.out.print(v.descricao + "\t" + v.dataVenda + "\t" + v.valor + "\t" + vendedor.nome + "\t" + v.comissao + "\t" + Double.toString(v.valor * (v.comissao / 100)) + "\n");
        }
    }
}
