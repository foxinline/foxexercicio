/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prova_Candidato4;

import Controllers.VendaController;
import Models.Venda;
import java.util.Scanner;

/**
 *
 * @author Candidato 4
 */
public class VendaGUI {
    public static void newVenda(Scanner scanner, VendaController vendaCtrl){
        Double valor = null;
        String dataVenda = null;
        String descricao = null;
        Double comissao = null;
        int idVendedor = 0;
        System.out.println("--------------------------------------------------------");
        System.out.println("\033[0;1m|\t Cadastrar nova Venda\t\t\t|");
        System.out.println("--------------------------------------------------------");
        System.out.println("Informe o valor da venda:");
        valor = scanner.nextDouble();
        System.out.println("Informe a data da venda(dd-mm-yy):");
        dataVenda = scanner.next();
        System.out.println("Informe a descrição da venda:");
        descricao = scanner.next();
        System.out.println("Informe a comissao:");
        comissao = scanner.nextDouble();
         System.out.println("Informe a id do Vendedor:");
        idVendedor = scanner.nextInt();
        vendaCtrl.inserirVenda(valor, dataVenda, descricao, comissao, idVendedor);
    }
    public static void showVenda(Scanner scanner, VendaController vendaCtrl){
        int option;
        do {
            System.out.println("--------------------------------------------------------");
            System.out.println("\033[0;1m|\t Controle de Vendas\t\t\t\t|");
            System.out.println("--------------------------------------------------------");
            System.out.println("|\t0 - Voltar\t\t\t\t\t|");
            System.out.println("|\t1 - Cadastrar novo\t\t\t\t|");
            System.out.println("|\t2 - Listar vendas\t\t\t\t|");
            System.out.println("|\t3 - Procurar Venda\t\t\t\t|");
            System.out.println("|\t4 - Atualizar dados\t\t\t\t|");
            System.out.println("|\t5 - Remover dados\t\t\t\t|");
            System.out.println("--------------------------------------------------------\n");
            System.out.println("Digite uma opção acima para gerenciar os dados:");
            option = scanner.nextInt();
            if(option == 0){
            System.out.println("\033[0;1mVoltando...");
            } else if(option == 1) {
                newVenda(scanner,vendaCtrl);                
            } else if(option == 2) {
                listVenda(vendaCtrl);
            } else if(option == 3) {
                findVenda(scanner, vendaCtrl);
            } else if(option == 4) {
                updateVenda(scanner, vendaCtrl);
            } else if(option == 5) {
                removerVenda(scanner, vendaCtrl);
            } else {
                System.out.println("Opção inválida");
            }
        } while(option != 0);
    }
    
    public static void listVenda(VendaController vendaCtrl){
        System.out.println("--------------------------------------------------------");
        System.out.println("\033[0;1m|\t Listar Vendas\t\t\t|");
        System.out.println("--------------------------------------------------------");
        vendaCtrl.listVendas();
    }
    
    public static void findVenda(Scanner scanner, VendaController vendaCtrl){
        System.out.println("--------------------------------------------------------");
        System.out.println("\033[0;1m|\t Procurar Vendas\t\t\t|");
        System.out.println("--------------------------------------------------------");
        System.out.println("Informe a id da Venda:");
        int idVenda = scanner.nextInt();
        Venda v = vendaCtrl.procurarVenda(idVenda);
        if (v == null) {
            System.out.println("\033[0;1midVenda não encontrada.");
        } else {
            System.out.println("\033[0;1mID\tValor\tData\tDescrição\tComissão (%)\tID do Vendedor (R$)");
            System.out.print(v.idVenda + "\t" + v.valor + "\t" + v.dataVenda + "\t" + v.descricao + "\t" + v.comissao + "\t" + v.idVendedor + "\n");
        }
    }
    
    public static void updateVenda(Scanner scanner, VendaController vendaCtrl){
        int idVenda = 0;
        int idVendedor = 0;
        Double valor = null;
        String dataVenda = null;
        String descricao = null;
        Double comissao = null;
        System.out.println("--------------------------------------------------------");
        System.out.println("\033[0;1m|\t Atualizar Venda\t\t\t|");
        System.out.println("--------------------------------------------------------");
        System.out.println("Informe a id da Venda:");
        idVenda = scanner.nextInt();
        System.out.println("Informe o valor da venda:");
        valor = scanner.nextDouble();
        System.out.println("Informe a data da venda(dd-mm-yy):");
        dataVenda = scanner.next();
        System.out.println("Informe a descrição da venda:");
        descricao = scanner.next();
        System.out.println("Informe a comissao:");
        comissao = scanner.nextDouble();
         System.out.println("Informe a id do Vendedor:");
        idVendedor = scanner.nextInt();
        vendaCtrl.atualizarVenda(idVenda, valor, dataVenda, descricao, comissao, idVendedor);
    }
    
    public static void removerVenda(Scanner scanner, VendaController vendaCtrl){
        System.out.println("--------------------------------------------------------");
        System.out.println("\033[0;1m|\t Remover Venda\t\t\t|");
        System.out.println("--------------------------------------------------------");
        System.out.println("Informe a id da Venda:");
        int idVenda = scanner.nextInt();
        vendaCtrl.removerVenda(idVenda);
    }
}
