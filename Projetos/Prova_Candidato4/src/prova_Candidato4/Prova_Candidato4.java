/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prova_Candidato4;

import Controllers.VendaController;
import Controllers.VendedorController;
import Models.Vendedor;
import Models.Venda;
import java.util.Scanner;

/**
 *
 * @author Candidato 4 - Publicou projeto no github
 */
public class Prova_Candidato4 {
    /**
     * @param args the command line arguments
     */
    public static void showMenu(){
        System.out.println("--------------------------------------------------------");
        System.out.println("\033[0;1m|\t Sistema de Controle de Vendas/Vendedores\t|");
        System.out.println("--------------------------------------------------------");
        System.out.println("|\t0 - Sair\t\t\t\t\t|");
        System.out.println("|\t1 - Vendedores\t\t\t\t\t|");
        System.out.println("|\t2 - Vendas\t\t\t\t\t|");
        System.out.println("--------------------------------------------------------\n");
    }
    
    public static void main(String[] args) {
        VendedorController vendedorCtrl = new VendedorController();
        VendaController vendaCtrl = new VendaController(vendedorCtrl);
        Scanner scanner = new Scanner(System.in);
        int option;
        do {
            showMenu();
            System.out.println("Digite uma opção acima para gerenciar os dados:");
            option = scanner.nextInt();
            if(option == 0){
            System.out.println("Saindo...");
            } else if(option == 1) {
                VendedorGUI.showVendedor(scanner,vendedorCtrl);
            } else if(option == 2) {
                System.out.println("Vendas");
                VendaGUI.showVenda(scanner,vendaCtrl);
            } else {
                System.out.println("Opção inválida");
            }
        } while(option != 0);
    }
}
