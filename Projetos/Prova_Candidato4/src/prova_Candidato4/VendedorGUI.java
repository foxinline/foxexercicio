/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prova_Candidato4;

import Controllers.VendedorController;
import Models.Vendedor;
import java.util.Scanner;

/**
 *
 * @author Candidato 4
 * 
 * This class is only for GUI drawing
 */
public class VendedorGUI {
    public static void newVendedor(Scanner scanner, VendedorController vendedorCtrl){
        String nome = null;
        String dataNascimento = null;
        String cpf = null;
        String cargo = null;
        System.out.println("--------------------------------------------------------");
        System.out.println("\033[0;1m|\t Cadastrar novo Vendedor\t\t\t|");
        System.out.println("--------------------------------------------------------");
        System.out.println("Informe o nome completo:");
        nome = scanner.next();
        System.out.println("Informe a data de nascimento(dd-mm-yy):");
        dataNascimento = scanner.next();
        System.out.println("Informe o CPF:");
        cpf = scanner.next();
        System.out.println("Informe o cargo:");
        cargo = scanner.next();
        vendedorCtrl.inserirVendedor(nome, dataNascimento, cpf, cargo);
    }
    public static void showVendedor(Scanner scanner, VendedorController vendedorCtrl){
        int option;
        do {
            System.out.println("--------------------------------------------------------");
            System.out.println("\033[0;1m|\t Controle de Vendedores\t\t\t\t|");
            System.out.println("--------------------------------------------------------");
            System.out.println("|\t0 - Voltar\t\t\t\t\t|");
            System.out.println("|\t1 - Cadastrar novo\t\t\t\t|");
            System.out.println("|\t2 - Listar todos\t\t\t\t|");
            System.out.println("|\t3 - Procurar Vendedor\t\t\t\t|");
            System.out.println("|\t4 - Atualizar dados\t\t\t\t|");
            System.out.println("|\t5 - Remover dados\t\t\t\t|");
            System.out.println("--------------------------------------------------------\n");
            System.out.println("Digite uma opção acima para gerenciar os dados:");
            option = scanner.nextInt();
            if(option == 0){
            System.out.println("\033[0;1mVoltando...");
            } else if(option == 1) {
                newVendedor(scanner,vendedorCtrl);                
            } else if(option == 2) {
                listVendedor(vendedorCtrl);
            } else if(option == 3) {
                findVendedor(scanner, vendedorCtrl);
            } else if(option == 4) {
                updateVendedor(scanner, vendedorCtrl);
            } else if(option == 5) {
                removerVendedor(scanner, vendedorCtrl);
            } else {
                System.out.println("Opção inválida");
            }
        } while(option != 0);
    }
    
    public static void listVendedor(VendedorController vendedorCtrl){
        System.out.println("--------------------------------------------------------");
        System.out.println("\033[0;1m|\t Listar Vendedores\t\t\t|");
        System.out.println("--------------------------------------------------------");
        vendedorCtrl.listarVendedores();
    }
    
    public static void findVendedor(Scanner scanner, VendedorController vendedorCtrl){
        System.out.println("--------------------------------------------------------");
        System.out.println("\033[0;1m|\t Procurar Vendedores\t\t\t|");
        System.out.println("--------------------------------------------------------");
        System.out.println("Informe a id do Vendedor:");
        int idVendedor = scanner.nextInt();
        Vendedor v = vendedorCtrl.procurarVendedor(idVendedor);
        if (v == null) {
            System.out.println("\033[0;1midVendedor não encontrado.");
        } else {
            System.out.println("\033[0;1midVendedor\tNome\tData de Nascimento\tCPF\tCargo\t");
            System.out.println(v.idVendedor + "\t" + v.nome + "\t" + "\t" + v.dataNascimento + "\t" + v.cpf + "\t" + v.cargo);
        }
    }
    
    public static void updateVendedor(Scanner scanner, VendedorController vendedorCtrl){
        int idVendedor = 0;
        String nome = null;
        String dataNascimento = null;
        String cpf = null;
        String cargo = null;
        System.out.println("--------------------------------------------------------");
        System.out.println("\033[0;1m|\t Atualizar Vendedor\t\t\t|");
        System.out.println("--------------------------------------------------------");
        System.out.println("Informe a id do Vendedor:");
        idVendedor = scanner.nextInt();
        System.out.println("Informe o novo nome completo:");
        nome = scanner.next();
        System.out.println("Informe a nova data de nascimento(dd-mm-yy):");
        dataNascimento = scanner.next();
        System.out.println("Informe o novo CPF:");
        cpf = scanner.next();
        System.out.println("Informe o novo cargo:");
        cargo = scanner.next();
        vendedorCtrl.atualizarVendedor(idVendedor, nome, dataNascimento, cpf, cargo);
    }
    
    public static void removerVendedor(Scanner scanner, VendedorController vendedorCtrl){
        System.out.println("--------------------------------------------------------");
        System.out.println("\033[0;1m|\t Remover Vendedores\t\t\t|");
        System.out.println("--------------------------------------------------------");
        System.out.println("Informe a id do Vendedor:");
        int idVendedor = scanner.nextInt();
        vendedorCtrl.removerVendedor(idVendedor);
    }
}
