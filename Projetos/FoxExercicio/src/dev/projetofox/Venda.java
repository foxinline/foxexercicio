package dev.projetofox;

import java.util.ArrayList;

public class Venda {
	// Declaração de atributos da classe
	
	public double valor;
	public String dataVenda;
	public String descricao;
	public String comissao;
	
	ArrayList<Venda> vendas = new ArrayList<Venda>();
	
	// mettodos da classe
	public void addVenda(Venda novo){
		// adiciona um nova venda 
		if(vendas.isEmpty())
			vendas.add(novo);
		else
			vendas.lastIndexOf(novo);
	}
	
	public void removeVenda(String cpf){
		// remove uma venda
		
		//obs.: buscar venda para ser removida dos dados
		Venda vendaR = this.buscarVendas(cpf);
		
		vendas.remove(vendaR);
	}
	
	public void atualizarVenda(int id){
		// atualiza uma venda
	}
	
	public Venda buscarVendas(String dataV){
		// busca por um venda
		for (Venda venda : vendas) 
			if(dataVenda.equals(dataV))
				return venda;

		return null;
		
	}
	
	public String listarVendas(){
		for (Venda venda : vendas) 
			return venda.toString();
		
		return "Nenhum Cadastro encontado!";
	}

	@Override
	public String toString() {
		return "Venda [valor=" + valor + ", dataVenda=" + dataVenda
				+ ", descricao=" + descricao + ", comissao=" + comissao
				+ ", vendas=" + vendas + "]";
	}
	
}
