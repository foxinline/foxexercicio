package dev.projetofox;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		
		Vendedor vendedor = new Vendedor(); // instacia da class Vendedor
		vendedor.vendedores = new ArrayList<Vendedor>(); // salva os dados de vendedor
		
		Venda venda = new Venda(); // instacia da class Venda
		ArrayList<Venda> vendas = new ArrayList<Venda>(); // salva os dados de vendedor
		int ops;
		
		do {
			
			
			System.out.println(" ====== Menu ====== ");
			System.out.println(" 1 = Cadastrar Vendedor ");
			System.out.println(" 2 = Cadastrar Venda ");
			System.out.println(" 3 = Listar Vendedores ");
			System.out.println(" 4 = Listar Vendas ");
			System.out.println(" 5 = Buscar Vendedor ");
			System.out.println(" 6 = Buscar Venda ");
			System.out.println(" 7 = Atualizar Vendedor ");
			System.out.println(" 8 = Atualizar Venda ");
			System.out.println(" 9 = Deletar Vendedor ");
			System.out.println("10 = Deletar Venda ");
			System.out.println(" 0 = Sair ");
			
			ops = in.nextInt();
			
			if(ops == 0) break;
				
			switch(ops){
			
				case 1:
					System.out.println(" ==== Cadastra vendedor ===== ");
					System.out.println("Digite o nome do vendedor: ");
					vendedor.setNome(in.next());
					
					System.out.println("Digite o cpf do vendedor: ");
					vendedor.setCpf(in.next());
					
					System.out.println("Informe a data de nacimento do vendedor: ");
					System.out.println("Digite o dia: ");
					vendedor.setDia(in.nextInt());
					
					System.out.println("Digite o m�s: ");
					vendedor.setMes(in.nextInt());
					
					System.out.println("Digite o ano: ");
					vendedor.setAno(in.nextInt());
					
					System.out.println("Informe o cargo do vendedor: ");
					vendedor.cargo = in.next();
					
					vendedor.addVendedor(vendedor);
					break;
				case 2: 
					System.out.println(" ==== Cadastro de Vendas ===== ");
					System.out.println("Digite o valor da venda: ");
					venda.valor = in.nextDouble();
					
					System.out.println("Digite a data da venda: ");
					venda.dataVenda = in.next();
					
					System.out.println("Informe um descri��o da venda ");
					venda.descricao = in.next();
					
					System.out.println("Digite o ano: ");
					venda.comissao = in.next();
					
					
					vendedor.setAno(in.nextInt());
					
					System.out.println("Informe o cargo do vendedor: ");
					vendedor.cargo = in.next();
				case 3: 
					vendedor.listarVendedor();
					System.out.println();
					break;
				default :
					System.out.println("Op��o n�o encontrada!");
			}
			
		} while (ops != 0);
		
		
		
		
		
	}

}
