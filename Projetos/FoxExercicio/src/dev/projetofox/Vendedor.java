package dev.projetofox;

import java.util.ArrayList;
import java.util.Arrays;

public class Vendedor extends Pessoa{ // importando heran�a de Pessoa  
	
	// Declara��o de atributos da class
	public String cargo;
	
	ArrayList<Vendedor> vendedores;

	// Metodos da class
	public void addVendedor(Vendedor novo){
		// adiciona um novo vendendor
		if(vendedores.isEmpty())
			vendedores.add(novo);
		else
			vendedores.lastIndexOf(novo);
	}
	
	public void removeVendedor(String cpf){
		//remove um vendedor
		
		// obs buscar o vendedor que deve ser excluido
		Vendedor removeV = this.buscarVendedor(cpf);
		vendedores.remove(removeV);
	}
	
	public void atualizarVendedor(String cpf){
		//atualiza dados do vendeder
		
		// obs buscar o vendedor que deve ser atualizado
		Vendedor removeV = this.buscarVendedor(cpf);
	}
	
	public Vendedor buscarVendedor(String cpf){
		// buscar por uma vendedor
		
		// busca por um venda
		for (Vendedor vendedor : vendedores) 
			if(cpf.equals(cpf))
				return vendedor;

		return null;
		
	}
	
	public void listarVendedor(){
		//lista todos os vendedores cadatrados
		
		if(vendedores.isEmpty())
			System.out.println("Nenhum Cadastro encontado!");
		else	
			for (Vendedor vendo : vendedores) 
				System.out.println(vendo.toString());
		
	}
	
	@Override
	public String toString() {
		return "Vendedor [Nome Vendedor: " + getNome() + ", Data de Nacimento: "
		+  getDia()+"/"+ getMes() +"/"+ getAno() + ", CPF: " + getCpf() + ", Cargo: " + cargo + "]";
	}
}
