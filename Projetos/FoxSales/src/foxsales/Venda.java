/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foxsales;

import java.math.BigDecimal;
import java.sql.Date;

/**
 *
 * @author Candidato 2
 */
public class Venda {
  private int IdVenda;
  private BigDecimal valor;
  private Date dataVenda;
  private String descricao;
  private Double comissao;

    /**
     * @return retorna o valor
     */
    public BigDecimal getValor() {
        return valor;
    }

    /**
     * @param valor parametro que passa o Valor para editar
     */
    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    /**
     * @return retorna a Data da Venda
     */
    public Date getDataVenda() {
        return dataVenda;
    }

    /**
     * @param dataVenda parametro que passa a Data da Venda para editar
     */
    public void setDataVenda(Date dataVenda) {
        this.dataVenda = dataVenda;
    }

    /**
     * @return retorna a descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao parametro que passa a descrição para editar
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * @return retorna a comissao
     */
    public Double getComissao() {
        return comissao;
    }

    /**
     * @param comissao parametro que passa a comissao para editar
     */
    public void setComissao(Double comissao) {
        this.comissao = comissao;
    }

    /**
     * @return retorna o ID da Venda
     */
    public int getIdVenda() {
        return IdVenda;
    }

    /**
     * @param IdVenda parametro que passa o ID da Venda para editar
     */
    public void setIdVenda(int IdVenda) {
        this.IdVenda = IdVenda;
    }
    
    
}
