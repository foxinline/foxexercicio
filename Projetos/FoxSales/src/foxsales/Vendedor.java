/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foxsales;

import java.sql.Date;


/**
 *
 * @author Candidato 2
 */
public class Vendedor {
  private int IdVendedor;
  private String nome;
  private Date dataNascimento;
  private String cpf;
  private String cargo;

    /**
     * @return retorna o nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome parametro que passa o nome para editar
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return retorna a Data de Nascimento
     */
    public Date getDataNascimento() {
        return dataNascimento;
    }

    /**
     * @param dataNascimento parametro que passa a Data de Nascimento para editar
     */
    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    /**
     * @return retorna o cpf
     */
    public String getCpf() {
        return cpf;
    }

    /**
     * @param cpf parametro que passa a CPF para editar
     */
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    /**
     * @return retorna o cargo
     */
    public String getCargo() {
        return cargo;
    }

    /**
     * @param cargo parametro que passa a cargo para editar
     */
    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    /**
     * @return retorna o Id do Vendedor
     */
    public int getIdVendedor() {
        return IdVendedor;
    }

    /**
     * @param IdVendedor parametro que passa o Id do Vendedor para editar
     */
    public void setIdVendedor(int IdVendedor) {
        this.IdVendedor = IdVendedor;
    }

    
}
