/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foxsales;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Candidato 2
 */
public class VendedorDAO {
    
    private Connection con;

    public VendedorDAO() throws Exception {
        this.con = ConexaoFactory.getConexao();
    }

    /**
     * Metodo responsavel por fazer o registro na base de dados.
     *
     * @param vendedor objeto que sera cadastrado no banco de dados.
     */
    public void cadastrar(Vendedor vendedor) {
        String sql = "insert into vendedor(nome, dataNascimento, cpf, cargo)values(?,?,?,?)";
        try (PreparedStatement preparestatement = con.prepareStatement(sql)) {

            preparestatement.setString(1, vendedor.getNome()); //substitui o ? pelo dado do usuario
            preparestatement.setDate(2, vendedor.getDataNascimento());
            preparestatement.setString(3, vendedor.getCpf());
            preparestatement.setString(4, vendedor.getCargo());

            //executando comando sql
            preparestatement.execute();
            preparestatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * Metodo que faz a alteração / update de um registro .
     *
     * @param venda objeto que sera alterado quando esse metodo for chamado.
     */
    public void alterar(Vendedor vendedor) {
        String sql = "update vendedor set nome = ?, dataNascimento = ?, cpf = ?, cargo = ? where IdVendedor = ?";
        try (PreparedStatement preparedStatement = con.prepareStatement(sql)) {
            preparedStatement.setString(1, vendedor.getNome());
            preparedStatement.setDate(2, vendedor.getDataNascimento());
            preparedStatement.setString(3, vendedor.getCpf());
            preparedStatement.setString(4, vendedor.getCargo());
            preparedStatement.setInt(5, vendedor.getIdVendedor());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo responsavel por excluir um registro atraves da chave unica que é o
     * ID.
     *
     * @param venda objeto que sera excluido da base a partir do seu ID.
     */
    public void excluir(Vendedor vendedor) {
        String sql = "delete from vendedor where idVendedor = ?";
        try (PreparedStatement preparedStatement = con.prepareStatement(sql)) {
            preparedStatement.setInt(1, vendedor.getIdVendedor());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que retorna uma lista de todos os registro do banco de dados.
     *
     * @return uma lista com todos os objetos venda que estejam registrados na
     * base de dados.
     */
    public List<Vendedor> buscarTodos() {
        List<Vendedor> listarVendedor = new ArrayList<>();

        String sql = "Select * from vendedor";
        Vendedor vendedor = null;

        try (PreparedStatement preparedStatement = con.prepareStatement(sql)) {

            ResultSet result = preparedStatement.executeQuery();

            /*
			 * Dentro do while estou verificando se tem registro no banco de dados, enquanto tiver registro vai 
			 * adcionando um a um na lista e no final fora do while retorna todos os registro encontrados. 
             */
            while (result.next()) {
                vendedor  = new Vendedor();
                vendedor.setIdVendedor(result.getInt("idVenda"));
                vendedor.setNome(result.getString("nome"));
                vendedor.setDataNascimento(result.getDate("dataNascimento"));
                vendedor.setCpf(result.getString("cpf"));
                vendedor.setCargo(result.getString("cargo"));

                // Adcionando cada registro encontro, na lista .
                listarVendedor.add(vendedor);
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return listarVendedor;
    }
    
}
