/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foxsales;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author Candidato 2
 */
public class ConexaoFactory {
    
    //Login do postgres
    private static final String LOGIN = "postgres";
 
   //Senha do postgres
   private static final String SENHA = "postgres";
 
   //Dados de caminho, porta e nome da base de dados que irá ser feita a conexão
   private static final String BANCO_URL = "jdbc:postgresql://localhost:5432/foxsales";
   /**
   * Cria uma conexão com o banco de dados Postgres utilizando o nome de usuário e senha fornecidos e endereço do banco de dados
   * @return uma conexão com o banco de dados
   * @throws Exception
   */
   public static Connection getConexao() throws Exception{
      Class.forName("org.postgresql.Driver"); //Faz com que a classe seja carregada pela JVM
 
      //Cria a conexão com o banco de dados
      Connection connection = DriverManager.getConnection(BANCO_URL, LOGIN, SENHA);
      
 
      return connection;
   }
   public static void main(String[] args) throws Exception{
 
      //Recupera uma conexão com o banco de dados
      Connection con = getConexao();
 
      //Testa se a conexão é nula
      if(con != null){
         System.out.println("Conexão obtida com sucesso!" + con);
         con.close();
      }
 
   }

}

