/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package foxsales;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Candidato 2
 */
public class VendaDAO {

    private Connection con;

    public VendaDAO() throws Exception {
        this.con = ConexaoFactory.getConexao();
    }

    /**
     * Metodo responsavel por fazer o registro na base de dados.
     *
     * @param venda objeto que sera cadastrado no banco de dados.
     */
    public void cadastrar(Venda venda) {
        String sql = "insert into venda(valor, datavenda, descricao, comissao)values(?,?,?,?)";
        try (PreparedStatement preparestatement = con.prepareStatement(sql)) {

            preparestatement.setBigDecimal(1, venda.getValor()); //substitui o ? pelo dado do usuario
            preparestatement.setDate(2, venda.getDataVenda());
            preparestatement.setString(3, venda.getDescricao());
            preparestatement.setDouble(4, venda.getComissao());

            //executando comando sql
            preparestatement.execute();
            preparestatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * Metodo que faz a alteração / update de um registro .
     *
     * @param venda objeto que sera alterado quando esse metodo for chamado.
     */
    public void alterar(Venda venda) {
        String sql = "update venda set valor = ?, datavenda = ?, descricao = ?, comissao = ? where idvenda = ?";
        try (PreparedStatement preparedStatement = con.prepareStatement(sql)) {
            preparedStatement.setBigDecimal(1, venda.getValor());
            preparedStatement.setDate(2, venda.getDataVenda());
            preparedStatement.setString(3, venda.getDescricao());
            preparedStatement.setDouble(4, venda.getComissao());
            preparedStatement.setInt(5, venda.getIdVenda());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo responsavel por excluir um registro atraves da chave unica que é o
     * ID.
     *
     * @param venda objeto que sera excluido da base a partir do seu ID.
     */
    public void excluir(Venda venda) {
        String sql = "delete from venda where idvenda = ?";
        try (PreparedStatement preparedStatement = con.prepareStatement(sql)) {
            preparedStatement.setInt(1, venda.getIdVenda());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que retorna uma lista de todos os registro do banco de dados.
     *
     * @return uma lista com todos os objetos venda que estejam registrados na
     * base de dados.
     */
    public List<Venda> buscarTodos() {
        List<Venda> listarVendas = new ArrayList<>();

        String sql = "Select * from venda";
        Venda venda = null;

        try (PreparedStatement preparedStatement = con.prepareStatement(sql)) {

            ResultSet result = preparedStatement.executeQuery();

            /*
			 * Dentro do while estou verificando se tem registro no banco de dados, enquanto tiver registro vai 
			 * adcionando um a um na lista e no final fora do while retorna todos os registro encontrados. 
             */
            while (result.next()) {
                venda = new Venda();
                venda.setIdVenda(result.getInt("idvenda"));
                venda.setValor(result.getBigDecimal("valor"));
                venda.setDataVenda(result.getDate("datavenda"));
                venda.setDescricao(result.getString("descricao"));
                venda.setComissao(result.getDouble("comissao"));

                // Adcionando cada registro encontro, na lista .
                listarVendas.add(venda);
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return listarVendas;

    }
}
