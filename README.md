# Exercício de Programação Foxinline

## Descrição do projeto

Implemente uma aplicação onde seja possível manter vendedores e as vendas que realizaram.
Deve ser possível fazer a listagem de todas as vendas exibindo o valor em R$ da comissão do vendedor.
Exemplo da listagem abaixo:

- Tabela 1

| Venda (Descrição) | Data       | Valor    | Vendedor         | Comissão (%)  |  Comissão (R$) |
| ------------------|------------|--------- | -----------------|---------------|----------------|
| Michael Kors 7    | 02/01/2017 | 1.523,00 | José             |  5            | 76,15          |
| Banana Republic 1 | 07/01/2018 | 2.102,50 | Letícia          |  10           | 210,25         |


## Stack
 - Java 7+

## Requisitos

- CRUD Vendedor
- CRUD Venda
- Listagem das vendas conforme Tabela 1

## Modelo

![Diagrama Exercício](https://bitbucket.org/foxinline/foxexercicio/raw/master/diagrama_desafio.png "Diagrama")


## Informações Importantes

-   Fica a critério do participante como será a interface para entrada e saída de dados.
    Podendo ser utilizada a padrão do Java (System.in e System.out)

-   O uso de bancos de dados é opcional.


## Bônus

- Código documentado e organizado
- Criar repositório Git público do projeto e enviar o link
- Utilização de padrões de projeto
- Utilização de interface gráfica
- Utilização de Banco de dados

